#include "inc.h"

int child(void* lp)
{
    std::cout << "[+] child pid" << getpid() << std::endl;

    std::cout << "[+] executing /bin/sh" << std::endl;
    system("/bin/sh");

    std::cout << "[+] executing /usr/bin/gedit" << std::endl;
    system("/usr/bin/gedit");

    return EXIT_SUCCESS;
}

int main(int argc, char** argv)
{
    // set creation flags to create a clone of the process in all separate namespaces.
    // effectively isolating it almost entirely from the host process, in its own contained area
    int flags =
        SIGCHLD |
        CLONE_NEWUTS | // create proc in new UTS namespace
        CLONE_NEWPID | // create proc in new PID namespace
        CLONE_NEWIPC | // create proc in new IPC namespace
        CLONE_NEWNS |  // create proc in new mount namespace
        CLONE_NEWNET;  // create proc in new network namespace

    // dynalloc a new stack
    auto stack = new (std::nothrow) char[4096];

    std::cout << "stack: " << std::hex << (void*)stack << std::endl;;

    child(nullptr);

    // clone the process into its own namespace    
    //pid_t child_pid = clone(child, (void*)(stack + 4096), flags, nullptr);

    // check that it worked
    //if(child_pid == -1)
    //{
    //    std::cerr << "[x] clone failed, run with sudo\n";
    //    exit(EXIT_FAILURE);
    //}

    // wait for clone to finish
    //waitpid(child_pid, nullptr, 0);

    // cleanup
    delete[] stack;

    return EXIT_SUCCESS;
}